import React from 'react';

const addClassNames = (
  className?: string,
  selectTargetElement?: () => HTMLElement | null | undefined
) => {
  if (!selectTargetElement) return;
  const targetEl = selectTargetElement();
  let classNames: string[] | null = null;
  if (targetEl && className) {
    classNames = className.split(' ');
    classNames.forEach(className => {
      !targetEl.classList.contains(className) &&
        targetEl.classList.add(className);
    });
  }
};

const removeClassNames = (
  className?: string,
  selectTargetElement?: () => HTMLElement | null | undefined
) => {
  if (!selectTargetElement) return;
  const targetEl = selectTargetElement();
  let classNames: string[] | null = null;
  if (targetEl && className) {
    classNames = className.split(' ');
    classNames.forEach(className => targetEl.classList.remove(className));
  }
};

export interface ClassNameInjectionProps {
  selectTargetElement: () => HTMLElement | null | undefined;
  className?: string;
  observeElement?: () => HTMLElement | null | undefined;
}

export const ClassNameInjection: React.FC<ClassNameInjectionProps> = ({
  selectTargetElement,
  className,
  observeElement,
}) => {
  React.useLayoutEffect(() => {
    let timerId: any | null = null;
    addClassNames(className, selectTargetElement);
    const observer = new MutationObserver(() => {
      addClassNames(className, selectTargetElement);
      timerId = setTimeout(() => {
        addClassNames(className, selectTargetElement);
      }, 50);
    });
    if (observeElement) {
      const observeEl = observeElement();
      if (observeEl) {
        observer.observe(observeEl, {
          attributes: true,
          childList: true,
          subtree: true,
        });
      }
    }
    return () => {
      observer.disconnect();
      timerId && clearTimeout(timerId);
      removeClassNames(className, selectTargetElement);
    };
  }, []);

  return null;
};
