import React from 'react';
import ReactDOM from 'react-dom';

export interface InjectionProps {
  selectTargetElement: () => Element | HTMLElement | null | undefined;
  containerTagName?: keyof HTMLElementTagNameMap;
  containerClassName?: string;
  className?: string;
  children: React.ReactNode;
  position?: InsertPosition;
}

export const Injection: React.FC<InjectionProps> = ({
  selectTargetElement,
  containerTagName = 'div',
  children,
  containerClassName,
  position = 'afterend',
}) => {
  const [containerEl, setContainerEl] = React.useState<null | HTMLElement>(
    null
  );
  React.useLayoutEffect(() => {
    const targetEl = selectTargetElement();
    let _containerEl: HTMLElement | null = null;
    if (targetEl) {
      _containerEl = document.createElement(containerTagName);
      _containerEl.style.width = '100%';
      // _containerEl.style.height = '100%';
      containerClassName && _containerEl.classList.add(containerClassName);
      // className && _containerEl.classList
      targetEl.insertAdjacentElement(position, _containerEl);
      setContainerEl(_containerEl);
    }
    return () => {
      if (_containerEl) {
        _containerEl.remove();
      }
    };
  }, []);
  if (containerEl === null) return null;
  return ReactDOM.createPortal(children, containerEl);
};
