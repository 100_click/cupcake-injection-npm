import { ClassNameInjection } from './ClassNameInjection';
import { Injection } from './Injection';
const injectionModule = { ClassNameInjection, Injection };

//test

export { ClassNameInjection, Injection };
export default injectionModule;
