import React from 'react';
import ReactDOM from 'react-dom';
import { Injection, ClassNameInjection } from 'extension-injection';
import { createUseStyles } from 'react-jss';

const pageContainerEl = document.createElement('div');
pageContainerEl.classList.add('extension-page-injection');
document.body.appendChild(pageContainerEl);

const useStyles = createUseStyles({
  app: {
  },
  injectedButton : {
    background: '#2f71e5',
    border: '3px solid red',
    borderRadius: '50%',
    cursor: 'pointer',
    width: '100px',
    height: '100px',
    position: 'fixed',
    bottom: '50px',
    right: '50px',
    zIndex: '100',
    '&:hover' : {
      background: 'red',
    }
  },
  text: {
    fontFamily: 'Arial',
    fontSize: '16px',
    color: 'white'
  }
});



const App: React.FC = () => {
  const classes = useStyles();
  return (
    <div className={classes.app}>
      <Injection
        position="beforeend"
        selectTargetElement={() => document.querySelector('body')}
        containerClassName="extension-portal-injection"
      >
        <button className={classes.injectedButton}>
          <p className={classes.text}>Injected button</p>
        </button>
      </Injection>
    </div>
  );
};

ReactDOM.render(
  <App/>,
  pageContainerEl
);
